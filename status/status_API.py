






'''
	pip install --requirement structures_pip.utf8 --target structures_pip --upgrade
'''

import sys
print (sys.argv)



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'structures_pip'
])

import pathlib
this_folder = pathlib.Path (__file__).parent.resolve ()

from os.path import dirname, join, normpath
propinquity = normpath (join (this_folder, "../treasury/structures/propinquity"))

if (len (sys.argv) >= 2):
	glob_string = propinquity + sys.argv [1]
else:
	glob_string = propinquity + '/**/API_status_*.py'

print ("glob:", glob_string)

import body_scan
scan = body_scan.start (
	glob_string = glob_string,

	module_paths = [	
		normpath (join (this_folder, "../treasury/structures")),
		normpath (join (this_folder, "../treasury/structures_pip"))
	],
	
	relative_path = propinquity,
	
	db_directory = normpath (join (this_folder, "DB")),
)



#
#
#