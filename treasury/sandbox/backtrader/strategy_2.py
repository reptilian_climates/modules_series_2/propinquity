



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

import backtrader as bt
import backtrader.feeds as btfeeds
from datetime import datetime, timedelta
import yfinance as yf

from strategies.strategy_1 import Strategy_1

symbol = "VOO"
end_orbit = datetime.today () 
start_orbit = end_orbit - timedelta (days = 20)
interval = "1d"

treasure_df = yf.download (
	symbol, 
	
	start = start_orbit, 
	end = end_orbit,
	interval = interval,
	
	auto_adjust = True
)


'''
data = bt.feeds.PandasData (
	dataname = treasure_df
)
'''




cerebro = bt.Cerebro ()
cerebro.addstrategy (Strategy_1)

cerebro.broker.setcash (100000)
cerebro.broker.setcommission (commission = 0.001)




data = bt.feeds.PandasData (
	dataname = treasure_df
)

# todate = datetime (2021, 6, 25))

cerebro.adddata(data)
#cerebro.run()


print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())

# Run over everything
cerebro.run ()

# Print out the final result
print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())


cerebro.plot()