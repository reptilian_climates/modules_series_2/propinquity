



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

import backtrader as bt
import backtrader.feeds as btfeeds
from datetime import datetime, timedelta
import yfinance as yf


symbol = "RUN"
end_orbit = datetime.today () 
start_orbit = end_orbit - timedelta (days = 20)
interval = "30m"

treasure_df = yf.download (
	symbol, 
	
	start = start_orbit, 
	end = end_orbit,
	interval = interval,
	
	auto_adjust = True
)


'''
data = bt.feeds.PandasData (
	dataname = treasure_df
)
'''


class TestStrategy (bt.Strategy):

	def log(self, txt, dt=None):
		''' Logging function fot this strategy'''
		dt = dt or self.datas[0].datetime.date(0)
		print('%s, %s' % (dt.isoformat(), txt))

	def __init__(self):
		# Keep a reference to the "close" line in the data[0] dataseries
		self.dataclose = self.datas[0].close

		# To keep track of pending orders
		self.order = None

	def notify_order(self, order):
		if order.status in [order.Submitted, order.Accepted]:
			# Buy/Sell order submitted/accepted to/by broker - Nothing to do
			return

		# Check if an order has been completed
		# Attention: broker could reject order if not enough cash
		if order.status in [order.Completed]:
			if order.isbuy():
				self.log('BUY EXECUTED, %.2f' % order.executed.price)
			elif order.issell():
				self.log('SELL EXECUTED, %.2f' % order.executed.price)

			self.bar_executed = len(self)

		elif order.status in [order.Canceled, order.Margin, order.Rejected]:
			self.log('Order Canceled/Margin/Rejected')

		# Write down: no pending order
		self.order = None

	def next(self):
		# Simply log the closing price of the series from the reference
		self.log('Close, %.2f' % self.dataclose[0])

		# Check if an order is pending ... if yes, we cannot send a 2nd one
		if self.order:
			return

		# Check if we are in the market
		if not self.position:

			# Not yet ... we MIGHT BUY if ...
			if self.dataclose[0] < self.dataclose[-1]:
					# current close less than previous close

					if self.dataclose[-1] < self.dataclose[-2]:
						# previous close less than the previous close

						# BUY, BUY, BUY!!! (with default parameters)
						self.log('BUY CREATE, %.2f' % self.dataclose[0])

						# Keep track of the created order to avoid a 2nd order
						self.order = self.buy()

		else:

			# Already in the market ... we might sell
			if len(self) >= (self.bar_executed + 5):
				# SELL, SELL, SELL!!! (with all possible default parameters)
				self.log('SELL CREATE, %.2f' % self.dataclose[0])

				# Keep track of the created order to avoid a 2nd order
				self.order = self.sell()


cerebro = bt.Cerebro ()
cerebro.addstrategy (TestStrategy)

cerebro.broker.setcash (100000)



data = bt.feeds.PandasData (
	dataname = treasure_df
)

# todate = datetime (2021, 6, 25))

cerebro.adddata(data)
#cerebro.run()


print('Starting Portfolio Value: %.2f' % cerebro.broker.getvalue())

# Run over everything
cerebro.run ()

# Print out the final result
print('Final Portfolio Value: %.2f' % cerebro.broker.getvalue())