


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

from datetime import datetime, timedelta
import yfinance as yf
import pandas_ta as ta
import matplotlib.pyplot as pyplot
import matplotlib.dates as mdates

symbol = "RUN"
end_orbit = datetime.today () 
start_orbit = end_orbit - timedelta (days = 20)
interval = "30m"

treasure_df = yf.download (
	symbol, 
	start = start_orbit, 
	end = end_orbit,
	interval = interval
)

treasure_df.ta.sma (length = 10, append = True)
treasure_df.ta.ema (length = 10, append = True)

print (treasure_df)


def plot (df):
	pyplot.figure (
		figsize = (10, 10)
	)

	'''
		https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.subplot.html
		subplot (nrows, ncols, index, **kwargs)
	'''
	pyplot.subplot (2, 2, 1)
	pyplot.title ("indicators")
	
	pyplot.plot (df.index, df ['Adj Close'], label = 'Adj Close', color = 'black')
	pyplot.plot (df.index, df ['EMA_10'], label = 'EMA 10', color = 'yellow')
	pyplot.plot (df.index, df ['SMA_10'], label = 'SMA 10', color = 'blue')
	
	pyplot.gca ().xaxis.set_major_formatter (mdates.DateFormatter ('%b%d')) 
	pyplot.xticks (rotation = 0, fontsize = 14)
	pyplot.yticks (fontsize = 14)
	pyplot.legend ()
	
	pyplot.tight_layout ()
	pyplot.show ()

plot (treasure_df)

summary = treasure_df.iloc [-1] [
	[
		'Adj Close',
		'EMA_10',
		'SMA_10'
	]
]

print (summary)
