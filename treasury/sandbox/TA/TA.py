
'''
	https://github.com/bukosabino/ta
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


from pandas_datareader import data as pdr
import yfinance as yf
import json
import pandas as pd
from ta.utils import dropna
from ta.volatility import BollingerBands
import pandas as pd
from ta import add_all_ta_features
from ta.utils import dropna


treasure = yf.Ticker ("aapl")

'''
	1d, 1h, 30m, 15m, 5m
	
	[
        "Open",
        "High",
        "Low",
        "Close",
        
		"Volume",
		
        "Dividends",
        "Stock Splits"
    ]
'''
interval = "1d"
#interval = "1h"
df = treasure.history (
	start = "2023-08-01", 
	end = "2023-11-13", 
	interval = interval
)

'''
df = df.rename (
	columns = {'two':'new_name'}
)
'''

print (df)



df = add_all_ta_features(
    df, open="Open", high="High", low="Low", close="Close", volume="Volume")
	
	





# Load datas
# df = pd.read_csv('ta/tests/data/datas.csv', sep=',')
df = pd.read_csv ('datas.csv', sep=',')
print (df)

# Clean NaN values
df = dropna(df)

# Initialize Bollinger Bands Indicator
indicator_bb = BollingerBands (
	close = df["Close"], 
	window = 20, 
	window_dev = 2
)

# Add Bollinger Bands features
df['bb_bbm'] = indicator_bb.bollinger_mavg ()
print (df)


df['bb_bbh'] = indicator_bb.bollinger_hband()
df['bb_bbl'] = indicator_bb.bollinger_lband()

# Add Bollinger Band high indicator
df['bb_bbhi'] = indicator_bb.bollinger_hband_indicator()

# Add Bollinger Band low indicator
df['bb_bbli'] = indicator_bb.bollinger_lband_indicator()

# Add Width Size Bollinger Bands
df['bb_bbw'] = indicator_bb.bollinger_wband()

# Add Percentage Bollinger Bands
df['bb_bbp'] = indicator_bb.bollinger_pband()

