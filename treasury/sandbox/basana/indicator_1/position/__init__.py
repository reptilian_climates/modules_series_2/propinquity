



from decimal import Decimal
import asyncio
import logging

from talipp.indicators import SMA

from basana.backtesting import charts
from basana.external.bitstamp import csv
import basana as bs
import basana.backtesting.exchange as backtesting_exchange



# The position manager is responsible for executing trades and managing positions.
class PositionManager:
	def __init__(self, exchange: backtesting_exchange.Exchange, position_amount: Decimal):
		assert position_amount > 0
		self._exchange = exchange
		self._position_amount = position_amount

	async def on_trading_signal(self, trading_signal: bs.TradingSignal):
		logging.info (
			"Trading signal: operation=%s pair=%s", 
			trading_signal.operation, 
			trading_signal.pair
		)
		
		try:
			# Calculate the order size.
			balances = await self._exchange.get_balances()
			if trading_signal.operation == bs.OrderOperation.BUY:
				_, ask = await self._exchange.get_bid_ask(trading_signal.pair)
				balance = balances[trading_signal.pair.quote_symbol]
				order_size = min(self._position_amount, balance.available) / ask
			else:
				balance = balances[trading_signal.pair.base_symbol]
				order_size = balance.available
				
			pair_info = await self._exchange.get_pair_info(trading_signal.pair)
			order_size = bs.truncate_decimal(order_size, pair_info.base_precision)
			
			if not order_size:
				return

			logging.info(
				"Creating %s market order for %s: amount=%s", 
				trading_signal.operation, 
				trading_signal.pair, 
				order_size
			)
			
			await self._exchange.create_market_order(trading_signal.operation, trading_signal.pair, order_size)
		
		except Exception as e:
			logging.error(e)
