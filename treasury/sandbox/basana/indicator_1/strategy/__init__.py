
from decimal import Decimal
import asyncio
import logging

from talipp.indicators import SMA

from basana.backtesting import charts
from basana.external.bitstamp import csv
import basana as bs
import basana.backtesting.exchange as backtesting_exchange


#
# The strategy implements the set of rules that 
# define when to enter or exit a trade based on market conditions.
#
class SMA_Strategy (bs.TradingSignalSource):
	def __init__ (self, dispatcher: bs.EventDispatcher, period: int):
		super ().__init__(dispatcher)
		self.sma = SMA(period)
		self._values = (None, None)

	async def on_bar_event (self, bar_event: bs.BarEvent):
		
		#
		# Feed the technical indicator.
		#
		value = float(bar_event.bar.close)
		self.sma.add_input_value(value)

		# Keep a small window of values to check if there is a crossover.
		self._values = (self._values[-1], value)

		# Is the indicator ready ?
		if len (self.sma) < 2:
			return

		# Price crossed below SMA ?
		if self._values[-2] >= self.sma[-2] and self._values[-1] < self.sma[-1]:
			self.push (
				bs.TradingSignal (
					bar_event.when, 
					bs.OrderOperation.SELL, 
					bar_event.bar.pair
				)
			)
		
		# Price crossed above SMA ?
		elif self._values[-2] <= self.sma[-2] and self._values[-1] > self.sma[-1]:
			self.push (
				bs.TradingSignal (
					bar_event.when, 
					bs.OrderOperation.BUY, 
					bar_event.bar.pair
				)
			)
