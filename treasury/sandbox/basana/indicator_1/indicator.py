


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../structures',
	'../../../structures_pip'
])


from decimal import Decimal
import asyncio
import logging

from talipp.indicators import SMA

from basana.backtesting import charts
from basana.external.bitstamp import csv
import basana as bs
import basana.backtesting.exchange as backtesting_exchange

'''
	datetime,open,high,low,close,volume
'''
CSV = "../bitstamp_btcusd_day_2015.csv"

'''
#	Date,Open,High,Low,Close,Adj Close,Volume

CSV = "VOO.csv"
'''

from strategy import SMA_Strategy
from position import PositionManager


async def main ():
	logging.basicConfig (
		level = logging.INFO, 
		format = "[%(asctime)s %(levelname)s] %(message)s"
	)

	event_dispatcher = bs.backtesting_dispatcher ()
	pair = bs.Pair ("BTC", "USD")
	exchange = backtesting_exchange.Exchange(
		event_dispatcher,
		initial_balances = {
			"BTC": Decimal (0), 
			"USD": Decimal (10000)
		}
	)
	exchange.set_pair_info (pair, bs.PairInfo(8, 2))

	# Connect the strategy to the bar events from the exchange.
	strategy = SMA_Strategy(event_dispatcher, 15)
	exchange.subscribe_to_bar_events(pair, strategy.on_bar_event)

	# Connect the position manager to the strategy signals.
	position_mgr = PositionManager(exchange, Decimal(1000))
	strategy.subscribe_to_trading_signals(position_mgr.on_trading_signal)

	# Load bars from CSV files.
	exchange.add_bar_source (
		csv.BarSource (pair, CSV, "1d")
	)

	# Setup the chart.
	chart = charts.LineCharts (exchange)
	chart.add_pair (pair)
	chart.add_pair_indicator ("SMA", pair, charts.DataPointFromSequence(strategy.sma))
	chart.add_portfolio_value ("USD")

	# Run the backtest.
	await event_dispatcher.run ()

	# Log balances.
	balances = await exchange.get_balances()
	for currency, balance in balances.items():
		logging.info("%s balance: %s", currency, balance.available)

	chart.show()

if __name__ == "__main__":
	asyncio.run (main ())