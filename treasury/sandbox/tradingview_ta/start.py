
'''
	This might be easier:
	
		https://www.tradingview.com/screener/
'''



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

'''
class Interval:
	INTERVAL_1_MINUTE = "1m"
	INTERVAL_5_MINUTES = "5m"
	INTERVAL_15_MINUTES = "15m"
	INTERVAL_30_MINUTES = "30m"
	INTERVAL_1_HOUR = "1h"
	INTERVAL_2_HOURS = "2h"
	INTERVAL_4_HOURS = "4h"
	INTERVAL_1_DAY = "1d"
	INTERVAL_1_WEEK = "1W"
	INTERVAL_1_MONTH = "1M"
'''

'''	
	if (5m >= 12):
	if (15m >= 12):
	if (1h >= 12):
	if (1d >= 12):
	
		

			5m		15m		1h		1d
	NOVA
'''

import json

from tradingview_ta import TA_Handler, Interval, Exchange

def find_summaries (treasures, intervals):
	print ()

	for interval in intervals:
		print ()
		print (interval)

		summaries = []

		for treasure in treasures:
			[ screener, exchange, symbol ] = treasure;

			treasure_data = TA_Handler (
				symbol = symbol,
				screener = screener,
				exchange = exchange,
				interval = interval
			)
			
			summary = treasure_data.get_analysis ().summary;
			buy = summary ["BUY"]
			sell = summary ["SELL"]
			neutral = summary ["NEUTRAL"]
			
			superiority = buy - sell;
			
			summaries.append ({
				"treasure": treasure,
				"summary": summary,
				"superiority": superiority
			})

		
		summaries = sorted (
			summaries, 
			key = lambda e : e ['superiority'],
			reverse = False
		)
		
		for summary in summaries:
			superiority = summary ['superiority']
			treasure = json.dumps (summary ['treasure'])

			print (f"  { superiority } { treasure }")


summaries = find_summaries ([
	[ "america", "AMEX", "SPY" ],
	[ "america", "NYSE", "NOVA" ],
	[ "america", "NASDAQ", "RUN" ],	
	
	[ "america", "NASDAQ", "OTLY" ],	
	[ "america", "AMEX", "GUSH" ],
	
	[ "america", "NASDAQ", "TOMZ" ]	
], [
	#Interval.INTERVAL_1_HOUR,
	Interval.INTERVAL_5_MINUTES
	
])




