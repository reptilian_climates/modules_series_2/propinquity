



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'structures_pip'
])

import botanical.paths.directory.find_and_replace_string as find_and_replace_string

import pathlib
from os.path import dirname, join, normpath
this_folder = pathlib.Path (__file__).parent.resolve ()
find_and_replace_string.start (
	glob_string = str (normpath (join (this_folder, "../structures/propinquity"))) + "/**/*",

	find = 'EVALUATION',
	replace_with = 'evaluation'
)