





def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


from datetime import datetime
import backtrader as bt

class SmaCross(bt.SignalStrategy):
    def __init__(self):
        sma1, sma2 = bt.ind.SMA(period=10), bt.ind.SMA(period=30)
        crossover = bt.ind.CrossOver(sma1, sma2)
        self.signal_add(bt.SIGNAL_LONG, crossover)

cerebro = bt.Cerebro ()
cerebro.addstrategy (SmaCross)

data0 = bt.feeds.YahooFinanceData (
	dataname = 'VOO.csv' 
)

'''
data0 = bt.feeds.YahooFinanceData (
	dataname = 'MSFT', 
	fromdate = datetime(2011, 1, 1),
	todate=datetime (2012, 12, 31)
)
'''

cerebro.adddata (data0)

cerebro.run ()
cerebro.plot ()