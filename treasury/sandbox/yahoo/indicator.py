






'''
	https://rapidapi.com/apidojo/api/yahoo-finance1
'''



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

from pandas_datareader import data as pdr
import yfinance as yf

import json



'''
	returns a pandas.DataFrame
'''
treasure = yf.Ticker ("aapl")

'''
	1d, 1h, 30m, 15m, 5m
	
	[
        "Open",
        "High",
        "Low",
        "Close",
        
		"Volume",
		
        "Dividends",
        "Stock Splits"
    ]
'''
interval = "1d"
treasure_historical = treasure.history (
	start = "2023-11-01", 
	end = "2023-11-13", 
	interval = interval
)

result = treasure_historical.to_json (orient = "split")
parsed = json.loads (result)
print (json.dumps (parsed, indent=4))

print (treasure_historical)

exit ()


yf.pdr_override () # <== that's all it takes :-)

# download dataframe
treasure = pdr.get_data_yahoo ("SPY", start = "2017-04-01", end = "2017-04-30")

print (treasure)

print (treasure.dividends)

