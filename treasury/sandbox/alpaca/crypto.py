
'''
	https://alpaca.markets/learn/algorithmic-trading-python-alpaca/
'''

'''
	https://pypi.org/project/alpaca-trade-api/
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

from alpaca.data.historical import CryptoHistoricalDataClient
from alpaca.data.requests import CryptoBarsRequest
from alpaca.data.timeframe import TimeFrame
from datetime import datetime

client = CryptoHistoricalDataClient ()

request_params = CryptoBarsRequest(
		symbol_or_symbols=["BTC/USD", "ETH/USD"],
		timeframe=TimeFrame.Day,
		start=datetime(2022, 7, 1)
 )

bars = client.get_crypto_bars (request_params)

print (bars)