
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

import json
fp = open ("/online academy/alpaca.markets/paper.JSON", "r")
paper = json.loads (fp.read ())
fp.close ()

# https://docs.alpaca.markets/reference/createorderforaccount