

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import plotly.express as px
import pandas as pd

df = pd.DataFrame (dict(
    x = [9, 4, 2, 8, 3],
    y = [84, 2742, 40, 45728 ]
))
df = df.sort_values (by="x")
fig = px.line (df, x="x", y="y", title="Chart") 
fig.show ()
