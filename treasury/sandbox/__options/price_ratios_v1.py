

'''
	volume ratios:
'''



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


'''
import json
fp = open ("/online ellipsis/tradier.com/ellipse.json", "r")
authorization = json.loads (fp.read ()) ["API"]
fp.close ()
'''

import propinquity.climate as climate
Tradier = climate.find ("Tradier")

symbol = "RUN"
'''
import propinquity.clouds.Tradier.v1.markets.options.expirations as options_expirations
expirations = options_expirations.discover ({
	"symbol": symbol,
	"authorization": authorization
})

print ("expirations:", expirations)


import propinquity.clouds.Tradier.v1.markets.options.chains as options_chains
chain = options_chains.discover ({
	"symbol": symbol,
	"expiration": expirations [0],
	"authorization": authorization
})

print ('chain:', chain)
'''

import propinquity.clouds.Tradier.procedures.options.combine as combine_options  
options_chains = combine_options.presently ({
	"symbol": symbol,
	"authorization": Tradier ['authorization']
})


'''
import pyjsonviewer
pyjsonviewer.view_data (json_data = options_chains)
'''

'''
https://www.datecalculator.org/day-of-the-year
{
	"symbol": "NOVA",
	"date": {
		#
		#	
		#
		"timezone": "UTC-0",
		"year": 2023,
		"month": "november",
		"day of month": 21
	}
}
'''
import json
f = open ("NOVA_options_chain.JSON", "w")
f.write (json.dumps (options_chains, indent = 4))
f.close ()

import propinquity.stats.aggregate_PC_ratio as aggregate_PC_ratio
evaluation = aggregate_PC_ratio.calc ({
	"expirations": options_chains
})

import json
print (json.dumps (evaluation, indent = 4))
