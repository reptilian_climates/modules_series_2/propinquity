

'''
	import propinquity.treasures.options.movement_calculator as option_movement_calculator
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

import propinquity.treasures.options.movement_calculator as option_movement_calculator

from fractions import Fraction

'''
	calls:
		break even
			= payment amount + strike price
			= 0.3 + 5
			= 5.3
		
		description 1:
			the strike price is 5,
			and the payment amount was 0.3
		
			the share price:
			
				share  revenue expenses
				price  
				4.3 -> $0.0 - $0.3 = -$0.3
				5.2 -> $0.2 - $0.3 = 0
				5.3 -> $0.3 - $0.3 = 0
				6.3 -> $1.3 - $0.3 = 0
					
'''
def retrieve_share_prices ():
	share_prices = []
	
	current = 0
	while (current <= 20):
		share_prices.append (float (current))
		current += Fraction (1, 4);
	
	return share_prices

def retrieve_multipliers (
	share_prices,
	table
):
	multipliers = {}

	for strike in table:	
		strike_price = Fraction (strike ["strike price"])
		option_price = Fraction (strike ["option price"])
	
		multipliers [ str (strike_price) ] = []
	
		for share_price in share_prices:		
			movement = option_movement_calculator.calc (
				option_type = "call",
				
				option_price = option_price,
				strike_price = strike_price,
				
				share_price = share_price
			)
			
			balance = movement.balance
			balance_multiplier = movement.balance_multiplier
			
			multipliers [ str (strike_price) ].append (float (balance_multiplier))

	return multipliers

share_prices = retrieve_share_prices ()

multipliers = retrieve_multipliers (
	share_prices = share_prices,
	table = [
		{
			"strike price": Fraction (7.5),
			"option price": Fraction (0.15),
		},
		{
			"strike price": Fraction (5),
			"option price": Fraction (0.3)
		},
		{
			"strike price": Fraction (2.5),
			"option price": Fraction (3.2),
		}
	]
)
	
import plotly.express as plotly_express
import pandas
df = pandas.DataFrame ({
	"share price": share_prices,
	
	** multipliers
})

figure = plotly_express.line (
	df, 
	x = "share price", 
	y = list (multipliers.keys ()), 
	title = "multipliers"
) 


figure.show ()

