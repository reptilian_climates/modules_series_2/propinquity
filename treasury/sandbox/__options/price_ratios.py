

'''
	maybe this is PCR?

	volume ratios:
'''



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import propinquity.climate as climate
Tradier = climate.find ("Tradier")

symbol = "BYND"
#symbol = "SPY"

import propinquity.clouds.Tradier.procedures.options.combine as combine_options  
options_chains = combine_options.presently ({
	"symbol": symbol,
	"authorization": Tradier ['authorization']
})

import propinquity.stats.aggregate_PC_ratio as aggregate_PC_ratio
options_price_supply_ratios = aggregate_PC_ratio.calc ({
	"expirations": options_chains
})

import json
print (json.dumps (options_price_supply_ratios, indent = 4))
