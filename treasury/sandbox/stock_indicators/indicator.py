



from stock_indicators import indicators

# fetch historical quotes from your feed (your method)
quotes = get_historical_quotes("MSFT")

# calculate 20-period SMA
results = indicators.get_sma (quotes, 20)

# use results as needed for your use case (example only)
for r in results:
    print (f"SMA on {r.date.date()} was ${r.sma or 0:.4f}")

