
'''
	https://docs.alpaca.markets/docs/getting-started-with-alpaca-market-data
'''

'''
									open      high       low     close        volume  trade_count          vwap
symbol  timestamp                                                                                                 
BTC/USD 2022-09-01 05:00:00+00:00  20055.79  20292.00  19564.86  20156.76   7141.975485     110122.0  19934.167845
        2022-09-02 05:00:00+00:00  20156.76  20444.00  19757.72  19919.47   7165.911879      96231.0  20075.200868
        2022-09-03 05:00:00+00:00  19924.83  19968.20  19658.04  19806.11   2677.652012      51551.0  19800.185480
        2022-09-04 05:00:00+00:00  19805.39  20058.00  19587.86  19888.67   4325.678790      62082.0  19834.451414
        2022-09-05 05:00:00+00:00  19888.67  20180.50  19635.96  19760.56   6274.552824      84784.0  19812.095982
        2022-09-06 05:00:00+00:00  19761.39  20026.91  18534.06  18724.59  11217.789784     128106.0  19266.835520

'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

import datetime

from alpaca.data.historical import CryptoHistoricalDataClient
from alpaca.data.requests import CryptoBarsRequest
from alpaca.data.timeframe import TimeFrame

import json

def retrieve ():
	client = CryptoHistoricalDataClient ()

	span = [
		datetime.datetime (2023, 11, 28),
		datetime.datetime (2023, 12, 2)
	]

	interval = TimeFrame.Day

	request_params = CryptoBarsRequest (
		symbol_or_symbols = ["BTC/USD"],
		timeframe = interval,
		start = span [0],
		end = span [1]
	)

	Alpaca_bars = client.get_crypto_bars (request_params)

	return Alpaca_bars

def Alpaca_df_to_dict_list ():
	Alpaca_bars = retrieve ();
	Alpaca_df_JSON = json.loads (Alpaca_bars.df.to_json (orient = "split"))

	columns = Alpaca_df_JSON ["columns"]
	index = Alpaca_df_JSON ["index"]
	data = Alpaca_df_JSON ["data"]
	
	proceeds = []
	
	s = 0;
	last_interval_index = len (index) - 1;
	last_column_index = len (columns) - 1;
	
	while s <= last_interval_index:
		interval = {
			"date": index [s][1]
		}
		
		s2 = 0
		while s2 <= last_column_index:
			interval [columns [ s2 ]] = data [s][s2]
	
			s2 += 1
	
		proceeds.append (interval)
		s += 1

	return proceeds

Alpaca_bars = retrieve ();
Alpaca_df_json = json.loads (Alpaca_bars.df.to_json (orient = "split"))

print ("Alpaca_df_json:", json.dumps (Alpaca_df_to_dict_list (), indent = 4))

exit ()

# Convert to dataframe
Alpaca_bars.df

print (Alpaca_bars.df)
print (Alpaca_bars.df.values.tolist ())

records = Alpaca_bars.df.to_dict ('records')

print (json.dumps ({
	"records": records
}, indent = 4))


import pandas
import numpy

from technical_indicators_lib import ATR

ATR_nudge = ATR ()
df = ATR_nudge.get_value_df (Alpaca_bars.df)


# Method 2: get the data by sending series values
'''
obv_values = obv.get_value_list (
	df["close"], 
	df["volume"]
)
'''

#df['date'] = df.index
df = df.reset_index()

df = df.rename (columns = {'ATR':'ATR super'})

#print (df)


import plotly.express as px
import pandas as pandas


fig = px.line (df, timestamp = "timestamp", open ="open", title="Chart") 
fig.show ()
