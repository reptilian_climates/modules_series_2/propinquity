


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../structures',
	'../../../structures_pip'
])

import yfinance as yf
import matplotlib.pyplot as plt

data = yf.Ticker('OTLY').history(period="max").reset_index()[["Date", "Open"]]

print ("data:", data)

plt.plot(data["Date"], data["Open"])

plt.show()
