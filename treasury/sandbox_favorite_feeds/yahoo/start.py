


'''


'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

from pandas_datareader import data as pdr
import yfinance as yf

import json


date_from = ""
date_to = ""

treasure = "otly"
interval = "1d"



'''
	1d, 1h, 30m, 15m, 5m
	
	[
        "Open",
        "High",
        "Low",
        "Close",
        
		"Volume",
		
        "Dividends",
        "Stock Splits"
    ]
'''


#
#	returns a pandas.DataFrame
treasure_story = yf.Ticker (treasure)


treasure_historical = treasure_story.history (
	start = "2023-11-01", 
	end = "2023-11-13", 
	interval = interval
)

result = treasure_historical.to_json (orient = "split")
parsed = json.loads (result)
print (json.dumps (parsed, indent=4))

print ("treasure_historical:")
print (treasure_historical)

'''
	https://stackoverflow.com/questions/40256338/calculating-average-true-range-atr-on-ohlc-data-with-python
'''
def wwma (values, n):
	"""
	 J. Welles Wilder's EMA 
	"""
	return values.ewm(alpha=1/n, adjust=False).mean()

def atr (df, n = 14):
	data = df.copy()

	print ('data:', data)

	high = data ["High"]
	low = data ["Low"]
	close = data ["Close"]

	data['tr0'] = abs(high - low)
	data['tr1'] = abs(high - close.shift())
	data['tr2'] = abs(low - close.shift())
	
	tr = data[['tr0', 'tr1', 'tr2']].max(axis=1)
	atr = wwma(tr, n)
	
	return atr
	
ATR = atr (treasure_historical)

print ("ATR:", ATR)
print (treasure_historical)