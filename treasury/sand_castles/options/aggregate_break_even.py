

'''
	python3 status.py 'stats/aggregate_break_even/_status/status_1.py'
'''

'''
	sources:
		https://www.nasdaq.com/market-activity/stocks/fslr/option-chain
'''
def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

import botanical.paths.files.scan.JSON as scan_JSON_path
import propinquity.stats.aggregate_break_even as aggregate_break_even
import propinquity.stats.aggregate_break_even.show as show_aggregate_break_even
	
import propinquity.clouds.Tradier.procedures.options.combine as combine_options  
import propinquity.climate as climate
import propinquity.treasures.options.shapes.shape_1 as shares_shape_1 

Tradier = climate.find ("Tradier")

symbol = "MIR"
# symbol = "RUN"

options_chains = combine_options.presently ({
	"symbol": symbol,
	"authorization": Tradier ["authorization"]
})	

shares_shape_1.assertions (options_chains)
	

proceeds = aggregate_break_even.calc ({
	"expirations": options_chains
})

import json
print ("proceeds:", json.dumps (proceeds, indent = 4))


show_aggregate_break_even.data (proceeds)

	
