
'''
	python3 status.py indicators/TR/_status/status_1.py
'''

def check_1 ():
	import propinquity.indicators.TR as TR_indicator
	
	places = [{
		"date string": "2023-11-28 06:00:00",
		"u timestamp": "1701151200",
		"date": 1701151200000,
		"open": 36901.035,
		"high": 38404.3875,
		"low": 36901.035,
		"close": 38056.5515,
		"volume": 2.416483534,
		"trade_count": 114.0,
		"vwap": 37965.0139822184
	},{
		"date string": "2023-11-29 06:00:00",
		"u timestamp": "1701237600",
		"date": 1701237600000,
		"open": 38053.85,
		"high": 38423.07,
		"low": 37602.688,
		"close": 37895.439,
		"volume": 10.206069493,
		"trade_count": 115.0,
		"vwap": 37894.9599072209
	}]
	
	TR_indicator.calc (
		#
		#	data 
		#
		places = places
	)
	
	import botany
	botany.show ({ "places": places })
	
	
	assert (places [1] ["true range"] == 820.3819999999978)
	
	
	
checks = {
	'check 1': check_1
}