
'''
	https://docs.cloud.coinbase.com/exchange/docs/sandbox
'''

'''
	https://exchange.coinbase.com/profile/api
'''


sandbox = "https://api-public.sandbox.exchange.coinbase.com"

import http.client
import json


conn = http.client.HTTPSConnection("api.exchange.coinbase.com")
conn.request (
	"POST", 
	"/orders", 
	"", 
	{
		'Content-Type': 'application/json'
	}
)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))